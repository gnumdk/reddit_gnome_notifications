# Dozed demo

Dozed maintenance API demo.

This simple app
* register a maintenance window on resume
* fetch last news item from GNOME Reddit
* release maintenance window when done

https://gitlab.gnome.org/gnumdk/dozed
