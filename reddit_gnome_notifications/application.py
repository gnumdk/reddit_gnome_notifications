# Copyright Cedric Bellegarde <cedric.bellegarde@adishatz.org>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import gi
gi.require_version("Soup", "3.0")
from gi.repository import GLib, Gio, Soup

from time import sleep
from threading import Thread
import json

SYSTEMD_DBUS_NAME="org.freedesktop.login1"
SYSTEMD_DBUS_PATH="/org/freedesktop/login1"
SYSTEMD_DBUS_INTERFACE="org.freedesktop.login1.Manager"

DOZED_DBUS_NAME="org.freedesktop.Dozed"
DOZED_DBUS_PATH="/org/freedesktop/Dozed"
DOZED_DBUS_INTERFACE="org.freedesktop.Dozed"

URI="https://www.reddit.com/r/gnome/new/.json"

TIMEOUT=600 * 1000

class Application(Gio.Application):
    def __init__(self, data_dir, app_id):
        Gio.Application.__init__(
            self,
            application_id=app_id,
            flags=Gio.ApplicationFlags.FLAGS_NONE
        )
        self.data = {}
        self.cancellable = Gio.Cancellable.new()
        self.loading_id = None
        self.connect("activate", self.on_activate)
        self.hold()
        try:
            self.logind = Gio.DBusProxy.new_for_bus_sync(
                Gio.BusType.SYSTEM,
                Gio.DBusProxyFlags.NONE,
                None,
                SYSTEMD_DBUS_NAME,
                SYSTEMD_DBUS_PATH,
                SYSTEMD_DBUS_INTERFACE,
                None
            )
            self.logind.connect(
                "g-signal",
                self.on_logind_signal
            )
        except Exception as e:
            self.logind = None
            print(e)

        try:
            self.dozed = Gio.DBusProxy.new_for_bus_sync(
                Gio.BusType.SYSTEM,
                Gio.DBusProxyFlags.NONE,
                None,
                DOZED_DBUS_NAME,
                DOZED_DBUS_PATH,
                DOZED_DBUS_INTERFACE,
                None
            )
        except Exception as e:
            self.dozed = None
            print(e)
        self.register(None)

    def load_news(self, maintenance_window_id):
        monitor = Gio.NetworkMonitor.get_default()

        if not monitor.get_network_available():
            return True

        self.loading_id = None
        self.cancellable = Gio.Cancellable.new()

        session = Soup.Session.new()
        msg = Soup.Message.new("GET", URI)
        session.send_and_read_async(
            msg,
            0,
            self.cancellable,
            self.on_send_and_read,
            maintenance_window_id
        )

    def notify(self, title):
        notification = Gio.Notification.new("Reddit")
        notification.set_body(title)
        self.send_notification(self.get_application_id(), notification)

    def register_maintenance_window(self):
        if self.dozed is not None:
            self.dozed.call("RegisterMaintenanceWindow",
                    GLib.Variant(
                        "(sb)",
                        (self.get_application_id(), True)
                    ),
                    Gio.DBusCallFlags.NONE,
                    -1,
                    None,
                    self.on_maintenance_window_registered,
                    None)

    def release_maintenance_window(self, maintenance_window_id):
        if self.dozed is not None:
                self.dozed.call("ReleaseMaintenanceWindow",
                                GLib.Variant(
                                    "(sx)",
                                    (self.get_application_id(),
                                     maintenance_window_id)
                                ),
                                Gio.DBusCallFlags.NONE,
                                -1,
                                None,
                                None,
                                None);

    def on_send_and_read(self, source, result, maintenance_window_id):
        try:
            data = source.send_and_read_finish(result).get_data()
            decode = json.loads(data.decode("utf-8"))

            for child in decode["data"]["children"]:
                title = child["data"]["title"]
                url = child["data"]["url"]
                if url not in self.data.keys():
                    self.data[url] = title
                    self.notify(title)
                    break

            self.release_maintenance_window(maintenance_window_id)
        except Exception as e:
            print(e)

    def on_maintenance_window_registered(self, source, result, user_data):
        maintenance_window_id = source.call_finish (result)[0]
        self.loading_id = GLib.timeout_add(1000, self.load_news, maintenance_window_id)

    def on_logind_signal(self, proxy, sender_name, signal_name, params):
        if signal_name != "PrepareForSleep":
            return

        self.maintenance_window_id = 0;
        self.cancellable.cancel()
        if self.loading_id is not None:
            GLib.source_remove(self.loading_id)
            self.loading_id = None

        about_to_suspend = params[0]

        if about_to_suspend is False:
            self.register_maintenance_window()

    def on_activate(self, application):
        pass

